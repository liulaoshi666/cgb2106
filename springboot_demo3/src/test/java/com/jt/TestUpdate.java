package com.jt;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestUpdate {

    @Autowired
    private UserMapper userMapper;

    /**
     * 1.将ID=231的数据 name改为 "xx"
     * Sql: update demo_user set name="xx" where id=231

     */
    @Test
    public void testUpdate(){
        User user = new User();
        user.setId(231).setName("中秋节快乐").setAge(10).setSex("男");
        //byId 表示ID只当作where条件.
        //其它不为null的属性 当作set条件
        userMapper.updateById(user);
    }

    /**
     * 2.将name="中秋节快乐" 改为 name="国庆快乐" age=40 sex="男"
     *  .update(arg1,arg2)
     *   arg1: 实体对象  set条件的数据
     *   arg2: updateWrapper 动态拼接where条件
     */
    @Test
    public void testUpdate2(){
        User user = new User();
        user.setName("国庆快乐").setAge(40).setSex("男");
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("name","中秋节快乐");
        userMapper.update(user,updateWrapper);
        System.out.println("更新操作成功!!!!");
    }
}
