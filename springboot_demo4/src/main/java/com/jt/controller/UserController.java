package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin    //开启跨域机制
//@Controller
//@ResponseBody //将服务端数据转化为JSON串返回
public class UserController {

    //编码规则: 面向接口编程 解耦
    @Autowired
    private UserService userService;

    /**
     * 查询所有的数据
     * URL: http://localhost:8090/findAll
     * 参数: 没有参数
     * 返回值: List<User>
     */
    @RequestMapping("/findAll")
    public List<User> findAll(){

        return userService.findAll();
    }

    /**
     *
     * 不同类型的请求注解说明:
     *  @PostMapping("")
     *  @PutMapping("")
     *  @DeleteMapping("")
     *  参数说明:
     *     1.参数名称必须与URL中的名称一致.
     *     2.SpringMVC可以根据用户的需求,自动的实现类型的转化
     *      底层实现: springmvc所有的参数默认都是String类型
     *      根据用户参数的类型,自动实现转化.
     *
     * URL地址:  http://localhost:8090/findUserById?id=1
     * 请求类型:  GET/DELETE  /POST/PUT
     * 参数: id=1
     * 返回值结果: User对象的json串
     *
     */
    //@RequestMapping(value = "/findUserById",method = RequestMethod.GET)
    @GetMapping("/findUserById")     //只允许接收get类型
    public User findUserById(Integer id){

        return userService.findUserById(id);
    }

    /**
     * 规则: SpringMVC 可以利用对象的方式接收
     * 底层实现: 参数name="xxx" 拼接set形成setName,之后检查对象中
     * 是否有对应的setName(), 如果匹配该方法,则为对象赋值.
     * 注意事项: 参数名称最好与属性名称一致
     *
     * URL地址: http://localhost:8090/findUserByNS?name=王昭君&sex=女
     * 参数：  name=xxx&sex=xx
     * 返回值：List<User>
     */
    @GetMapping("/findUserByNS")
    public List<User> findUserByNS(User user){

        return userService.findUserByNS(user);
    }

    /**
     * 后端服务器接收规则:
     *     1.参数与参数之后使用 /分隔
     *     2.参数的位置一旦确定,一般不变.
     *     3.接收的参数使用 {形参变量}
     *     4.使用@PathVariable 接收
     *     5.如果参数有多个建议使用对象接收  参数必须与属性一致,SpringMVC自动封装
     * 注意事项: 如果名称不统一,则需要转化 具体如下:
     *          @PathVariable("name") String username
     *
     * url: http://localhost:8090/user/貂蝉/10
     * 参数: name/age
     * 返回值: List<User>
     */

    @GetMapping("/user/{name}/{age}")
    public List<User> findUserByNA(User user){

        return userService.findUserByNA(user);
    }




   /*
    说明: restFul写法1
    @GetMapping("/user/{name}/{age}")
    public List<User> findUserByNA(@PathVariable String name,
                                   @PathVariable Integer age){
        System.out.println(name);
        System.out.println(age);

        return null;
    }*/

    /**
     * 规则: 如果参数使用,号分隔,则SpringMVC可以自动的转化为数组.
     * 查询多个用户
     * URL: http://localhost:8090/getUserByIds?ids=1,3,4,5
     * 参数: ids = 1,3,4,5
     * 返回值: List<User>
     */
    @GetMapping("/getUserByIds")
    public List<User> getUserByIds(Integer[] ids){

        return userService.getUserByIds(ids);
    }

}
