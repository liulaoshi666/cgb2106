package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/axios")
public class AxiosController {

    @Autowired
    private UserService userService;

    /**
     * URL: http://localhost:8090/axios/getUserById?id=1
     * 参数: id=1
     * 返回值: User对象
     */
    @GetMapping("/getUserById")
    public User getUserById(Integer id){

        return userService.findUserById(id);
    }

    /**
     * GET测试2: restFul结构接收参数
     * URL: http://localhost:8090/axios/user/女/18
     * 参数: sex/age
     * 返回值: List集合
     */
    @GetMapping("/user/{sex}/{age}")
    public List<User> getUserBySA(User user){

        return userService.getUserBySA(user);
    }

    /**
     * GET测试3: 利用对象的方式接收参数
     * URL: http://localhost:8090/axios/getUser?name=xx&sex=xx&age=xx
     * 参数: name/age/sex
     * 返回值: List<User>
     */
    @GetMapping("/getUser")
    public List<User> getUser(User user){

        return userService.getUser(user);
    }

    /**
     * 需求: 根据ID删除数据
     * URL: http://localhost:8090/axios/deleteById?id=232
     * 参数: id=232
     * 返回值: 字符串 删除数据成功!!!
     */
    @DeleteMapping("/deleteById")
    public String deleteById(Integer id){

        userService.deleteById(id);
        return "删除数据成功!!!";
    }

    /*
     * 需求: 实现用户入库操作
     * URL: http://localhost:8090/axios/saveUser
     * 参数: {name: "小燕子", age: 18, sex: "女"}
     * 返回值: String 新增成功!!!
     * 难点:
     *      json 互转  user对象
     *      1. user对象转化为json   @ResponseBody
     *      2. json转化为user对象   @RequestBody
     */
    @PostMapping("/saveUser")
    public String saveUser(@RequestBody User user){

        userService.saveUser(user);
        return "新增用户成功!!!";
    }

    /**
     * 需求: 根据id修改数据
     * URL: http://localhost:8090/axios/updateUser
     * 参数: {id: 238,name="小鬼当家",age=4,sex=男}
     * 返回值: 更新成功
     */
    @PutMapping("/updateUser")
    public String updateUser(@RequestBody User user){

        userService.updateUser(user);
        return "数据更新成功";
    }

    /**
     * 需求: 根据name修改数据
     * URL: http://localhost:8090/axios/updateUserByName/小燕子
     * 参数: name条件, {name: "小鬼当家",age: 18,sex: "男"}
     * 返回值: 修改成功
     * 注意事项:  restFul可以为对象的属性赋值.
     *          注意restFul的名称不要与属性重名.否则会覆盖
     *          引发BUG
     */
    @PutMapping("/updateUserByName/{whereName}")
    public String updateByName(@RequestBody User user,
                               @PathVariable String whereName){

        userService.updateUserByName(user,whereName);
        return "修改数据成功2222";
    }

}
