package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
//@Mapper //将该接口交给Spring管理,spring创建对象
public interface UserMapper {

    //查询所有的user用户
    List<User> findAll();

    /**
     *  规则:
     *      1.Mybatis 原生只支持单值传参
     *      2.如果是多值,则想办法封装为单值
     *          方法:  1.封装为对象
     *                2.封装为Map集合<K,V> ={minAge: 18,maxAge:100}
     *  补充说明: 现在mybatis版本 如果多值传参 则自动封装为Map集合
     *           Key:参数名称  value: 参数的值
     */
    //List<User> findWhere(String name,Integer age,String sex);
    List<User> findWhere(User user);
    List<User> findAge(int minAge,int maxAge);

    List<User> findName(String name);
    //List<User> findAge(@Param("minAge") int minAge,@Param("maxAge") int maxAge);
}
