package com.jt.test;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestMybatis {

    @Autowired
    private UserMapper userMapper; //必须有对象!!!!

    @Test
    public void test01(){

        List<User> userList = userMapper.findAll();
        System.out.println(userList);
    }

    /**
     * 以mybatis的方式实现数据库查询
     * 1.实现user入库操作   insert into
     * 2.update  将name="阿富汗" 改为 name="塔利班"
     * 3.delete  将name="塔利班"数据删除.
     * 4.select  查询 name="小乔" 并且 性别 ="女"
     * 5.select  查询age < 18岁  性别="女"
     * 6.select  查询  name包含 '君'字的数据
     * 7.select  查询  sex="女"  按照年龄倒序排列.
     * 8.根据 name/sex 不为null的数据查询. 动态Sql!!
     *   name="xxx" sex=null
     *   name="xxx" sex="xx"
     *   name=null  sex=null
     */

    /**
     * Mybatis 参数传递说明
     * 查询: sex="男" age > 18岁
     */
    @Test
    public void testMybatis(){
        //参数使用对象进行封装
        User user = new User();
        user.setSex("男").setAge(18);
        List<User> userList = userMapper.findWhere(user);
        System.out.println(userList);
    }

    /**
     * Mybatis 参数传递说明
     * 查询: age < 18  or age > 100
     * 思考: 1.是否可以封装为对象??  不可以!!
     *
     */
    @Test
    public void testMybatis2(){
        int minAge = 18;
        int maxAge = 100;
        List<User> userList = userMapper.findAge(minAge,maxAge);
        System.out.println(userList);
    }

    /**
     * Mybatis 参数传递说明
     * 查询name="王昭君"
     */
    @Test
    public void testMybatis3(){
        String name = "王昭君";
        List<User> userList = userMapper.findName(name);
        System.out.println(userList);
    }


}
